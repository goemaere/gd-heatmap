gd-heatmap
==========

GD-Heatmap is a heatmap generation library using PHP and GD.

Take a look at the test.php script to get started.

Note: this is not my code. Just forked it to do some minor changes.
All kudos go to https://github.com/xird/gd-heatmap